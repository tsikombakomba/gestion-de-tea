<?php
include "Connection.php";

function checkLogin($user,$pwd)
  {
     $requete="SELECT idUser,isAdmin FROM user where user='%s' and pwd='%s'";
     $query=sprintf($requete,$user,$pwd);
     $verification=mysqli_query(DbConnect(),$query);
     $count=mysqli_num_rows($verification);
     if($count > 0)
     {
        $resultats=mysqli_fetch_assoc($verification);
        $user=$resultats; 
        return $user;
     }
     else
     {
        return $count;
     }
  }
function getThe()
{
   $requete="SELECT * FROM the";
   $verification=mysqli_query(DbConnect(),$requete);
   $indice=0;
   while($resultats=mysqli_fetch_assoc($verification))
   {
     $valiny[$indice]=$resultats;
     $indice++;
   }
 return $valiny;  
}
function getInfoUser($idUser)
{
   $requete="SELECT * FROM user where  idUser=".$idUser;
   $verification=mysqli_query(DbConnect(),$requete);
   $resultats=mysqli_fetch_assoc($verification);
   return $resultats;  
}
function getParcelles()
{
   $requete="SELECT * FROM v_parcelle";
   $verification=mysqli_query(DbConnect(),$requete);
   $indice=0;
   while($resultats=mysqli_fetch_assoc($verification))
   {
     $valiny[$indice]=$resultats;
     $indice++;
   }
 return $valiny; ;
}
function getCueilleur()
{
   $requete="SELECT * FROM cueilleurs";
   $verification=mysqli_query(DbConnect(),$requete);
   $indice=0;
   while($resultats=mysqli_fetch_assoc($verification))
   {
     $valiny[$indice]=$resultats;
     $indice++;
   }
 return $valiny; 
}
function getSalaire()
{
  $requete="SELECT *,(rendement*prix) as salaire FROM v_parcelle";
  $verification=mysqli_query(DbConnect(),$requete);
  $indice=0;
   while($resultats=mysqli_fetch_assoc($verification))
   {
     $valiny[$indice]=$resultats;
     $indice++;
   }
  return $valiny;  
}
function getCategoryDepense()
{
  $requete="SELECT * FROM depenses";
  $verification=mysqli_query(DbConnect(),$requete);
  $indice=0;
   while($resultats=mysqli_fetch_assoc($verification))
   {
     $valiny[$indice]=$resultats;
     $indice++;
   }
  return $valiny;  
}
function getRendementById($idParcelle)
{
  $requete="SELECT rendement FROM v_parcelle where  idParcelle=".$idParcelle;
  $verification=mysqli_query(DbConnect(),$requete);
  $resultats=mysqli_fetch_assoc($verification);
  return $resultats;  
}
function getIdTheByParcelle($idParcelle)
{
  $requete="SELECT idThe FROM v_parcelle where  idParcelle=".$idParcelle;
  $verification=mysqli_query(DbConnect(),$requete);
  $resultats=mysqli_fetch_assoc($verification);
  return $resultats;  
}
function substractPoidsRendement($poid,$idThe)
{
  $requete="SELECT (('%d'*occupation)/surface) as rendement FROM v_parcelle where  idThe=".$idThe;
  $query=sprintf($requete,$poid);
  $verification=mysqli_query(DbConnect(),$query);
  $resultats=mysqli_fetch_assoc($verification);
  $update="UPDATE the set rendement=rendement-'%f' where idThe=".$idThe;
  $commit=sprintf($update,$resultats['rendement']);
  $insert=mysqli_query(DbConnect(),$commit);
}
function updateTea($idThe,$prix)
{
  $update="UPDATE the set prix='%d' where idThe=".$idThe;
  $commit=sprintf($update,$prix);
  $insert=mysqli_query(DbConnect(),$commit);
}
function updateMalus($idCueilleur,$malus)
{
  $update="UPDATE cueilleurs set malus=malus+'%d' where idCueilleur=".$idCueilleur;
  $commit=sprintf($update,$malus);
  $insert=mysqli_query(DbConnect(),$commit);
}
function updateBonus($idCueilleur,$bonus)
{
  $update="UPDATE cueilleurs set bonus=bonus+'%d' where idCueilleur=".$idCueilleur;
  $commit=sprintf($update,$bonus);
  $insert=mysqli_query(DbConnect(),$commit);
}
function updateMinus($idCueilleur,$minus)
{
  $update="UPDATE cueilleurs set minus=minus+'%d' where idCueilleur=".$idCueilleur;
  $commit=sprintf($update,$minus);
  $insert=mysqli_query(DbConnect(),$commit);
}
function getMois()
{
  $requete="SELECT * FROM mois";
  $verification=mysqli_query(DbConnect(),$requete);
  $indice=0;
  while($resultats=mysqli_fetch_assoc($verification))
  {
    $valiny[$indice]=$resultats;
    $indice++;
  }
 return $valiny;
}
function updateMoisDeRegeneration($idMois,$isRegenerating)
{
  $update="UPDATE mois set isRegenerating='%d' where idMois=".$idMois;
  $commit=sprintf($update,$isRegenerating);
  $insert=mysqli_query(DbConnect(),$commit);
}
function calculNombreJours($dateDebut, $dateFin) 
{
  $joursOuvrables = 0;
  $jourCourant = strtotime($dateDebut);
  while ($jourCourant <= strtotime($dateFin)) 
  {
      $joursOuvrables++;
      $jourCourant = strtotime('+1 day', $jourCourant);
  }
  return $joursOuvrables;
}
function getSalaireCeuilleurById($idCueilleur)
{
  $requete="SELECT *,(rendement*prix) as salaire FROM v_parcelle where idCueilleur=".$idCueilleur;
  $verification=mysqli_query(DbConnect(),$requete);
  $resultats=mysqli_fetch_assoc($verification);
  return $resultats;  
}
function getEstimationSalaire($idCueilleur,$dateDebut,$dateFin)
{
    $salaire=getSalaireCeuilleurById($idCueilleur);
    $nbJours=calculNombreJours($dateDebut,$dateFin);
    return $salaire["salaire"]*$nbJours;
}
?>
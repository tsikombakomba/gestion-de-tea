create database tea;
use tea;

drop table parcelle;
drop table user;
drop table the;
drop table mois;
drop table cueilleurs;
drop table depenses;

CREATE Table user(idUser int primary key auto_increment ,username varchar(50),pwd varchar(20),isAdmin boolean);
CREATE Table the(idThe int primary key auto_increment,nom varchar(50),occupation double,rendement double,date date);
CREATE Table cueilleurs(idCueilleur int primary key auto_increment,nom varchar(50),genre char,dtn date,rendement int);
CREATE Table parcelle(idParcelle int primary key auto_increment,surface double,idThe int,idCueilleur int);
CREATE Table depenses(idDepense int primary key auto_increment,nom varchar(50));
CREATE table mois(idMois int primary key auto_increment,nom varchar(20),isRegenerating boolean);

alter table  the add column rendementInitial varchar(50);
alter table  the add column prix int;
alter table  cueilleurs add column bonus int;
alter table  cueilleurs add column malus int;
alter table  cueilleurs add column minus int;

alter table parcelle add foreign key(idthe) references the(idthe);
alter table parcelle add foreign key(idCueilleur) references cueilleurs(idCueilleur);
alter table mois add constraint unique(nom);
alter table depenses add constraint unique(nom);

create or replace view v_parcelle as select cueilleurs.idCueilleur,idParcelle,surface,parcelle.idThe,the.nom as the,occupation,the.rendement,prix,cueilleurs.nom as cueilleurs from parcelle join the on parcelle.idThe=the.idThe join cueilleurs on parcelle.idCueilleur=cueilleurs.idCueilleur;

insert into user(username,pwd,isAdmin) values('ronny','ronny1234',false);
insert into user(username,pwd,isAdmin) values('admin','admin1234',true);

insert into the(nom,occupation,rendement,rendementInitial,prix) values('jasmin',4,2,2,10000,'2024-01-01');
insert into the(nom,occupation,rendement,rendementInitial,prix) values('rouge',3,5,5,30000,'2024-01-01');
insert into the(nom,occupation,rendement,rendementInitial,prix) values('vert',3,10,10,20000,'2024-01-01');

insert into cueilleurs(nom,dtn,genre,rendement,bonus,malus,minus) values('Jean glace','2000-05-02','m',12,0,0,0);
insert into cueilleurs(nom,dtn,genre,rendement,bonus,malus,minus) values('Natsipy','2002-03-01','m',11,0,0,0);
insert into cueilleurs(nom,dtn,genre,rendement,bonus,malus,minus) values('Ravao','1999-05-06','f',9,0,0,0);

insert into parcelle(surface,idThe,idCueilleur) values(600,1,3);
insert into parcelle(surface,idThe,idCueilleur) values(500,2,2);
insert into parcelle(surface,idThe,idCueilleur) values(650,3,1);

insert into depenses(nom) values('carburant');
insert into depenses(nom) values('Manger');
insert into depenses(nom) values('Engrais');
insert into depenses(nom) values('Outils');

insert into mois(nom,isRegenerating) values('janvier',false);
insert into mois(nom,isRegenerating) values('fevrier',false);
insert into mois(nom,isRegenerating) values('mars',false);
insert into mois(nom,isRegenerating) values('avril',false);
insert into mois(nom,isRegenerating) values('mais',false);
insert into mois(nom,isRegenerating) values('juin',false);
insert into mois(nom,isRegenerating) values('juillet',false);
insert into mois(nom,isRegenerating) values('aout',false);
insert into mois(nom,isRegenerating) values('septembre',false);
insert into mois(nom,isRegenerating) values('octobre',false);
insert into mois(nom,isRegenerating) values('novembre',false);
insert into mois(nom,isRegenerating) values('decembre',false);

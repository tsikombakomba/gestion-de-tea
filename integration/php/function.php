<?php
include "Connection.php";

function checkLogin($user,$pwd)
  {
     $requete="SELECT idUser,isAdmin,username FROM user where username='%s' and pwd='%s'";
     $query=sprintf($requete, $user, $pwd);
     $verification=mysqli_query(DbConnect(),$query);
     $count=mysqli_num_rows($verification);
     if($count > 0)
     {
        $resultats=mysqli_fetch_assoc($verification);
        $user=$resultats; 
        return $user;
     }
     else
     {
        return $count;
     }
  }
   function getThe()
   {
      $requete="SELECT * FROM the";
      $verification=mysqli_query(DbConnect(),$requete);
      $indice=0;
      while($resultats=mysqli_fetch_assoc($verification))
      {
      $valiny[$indice]=$resultats;
      $indice++;
      }
   return $valiny;  
   }
   function getInfoUser($idUser)
   {
      $requete="SELECT * FROM user where  idUser=".$idUser;
      $verification=mysqli_query(DbConnect(),$requete);
      $resultats=mysqli_fetch_assoc($verification);
      return $resultats;  
   }
   function getParcelles()
   {
      $requete="SELECT * FROM v_parcelle";
      $verification=mysqli_query(DbConnect(),$requete);
      $indice=0;
      while($resultats=mysqli_fetch_assoc($verification))
      {
      $valiny[$indice]=$resultats;
      $indice++;
      }
   return $valiny; ;
   }
   function getCueilleur()
   {
      $requete="SELECT * FROM cueilleurs";
      $verification=mysqli_query(DbConnect(),$requete);
      $indice=0;
      while($resultats=mysqli_fetch_assoc($verification))
      {
      $valiny[$indice]=$resultats;
      $indice++;
      }
   return $valiny; 
   }
   function getSalaire()
   {
   $requete="SELECT *,(rendement*prix) as salaire FROM v_parcelle";
   $verification=mysqli_query(DbConnect(),$requete);
   $indice=0;
      while($resultats=mysqli_fetch_assoc($verification))
      {
      $valiny[$indice]=$resultats;
      $indice++;
      }
   return $valiny;  
   }
   function getCategoryDepense()
   {
   $requete="SELECT * FROM depenses";
   $verification=mysqli_query(DbConnect(),$requete);
   $indice=0;
      while($resultats=mysqli_fetch_assoc($verification))
      {
      $valiny[$indice]=$resultats;
      $indice++;
      }
   return $valiny;  
   }
   function getRendementById($idParcelle)
   {
   $requete="SELECT rendement FROM v_parcelle where  idParcelle=".$idParcelle;
   $verification=mysqli_query(DbConnect(),$requete);
   $resultats=mysqli_fetch_assoc($verification);
   return $resultats;  
   }
   function getIdTheByParcelle($idParcelle)
   {
   $requete="SELECT idThe FROM v_parcelle where  idParcelle=".$idParcelle;
   $verification=mysqli_query(DbConnect(),$requete);
   $resultats=mysqli_fetch_assoc($verification);
   return $resultats;  
   }
   function substractPoidsRendement($poid,$idThe)
   {
      $requete="SELECT ((%d*occupation)/surface) as rendement FROM v_parcelle where  idThe=".$idThe;
      $query=sprintf($requete,$poid);
      $verification=mysqli_query(DbConnect(),$query);
      $resultats=mysqli_fetch_assoc($verification);
      $update="UPDATE the set rendement=rendement-'%f' where idThe=".$idThe;
      $commit=sprintf($update,$resultats['rendement']);
      $insert=mysqli_query(DbConnect(),$commit);
      return $resultats;
   }
   function updateTea($idThe,$prix)
   {
   $update="UPDATE the set prix='%d' where idThe=".$idThe;
   $commit=sprintf($update,$prix);
   $insert=mysqli_query(DbConnect(),$commit);
   }
   function updateMalus($idCueilleur,$malus)
   {
   $update="UPDATE cueilleurs set malus=malus+'%d' where idCueilleur=".$idCueilleur;
   $commit=sprintf($update,$malus);
   $insert=mysqli_query(DbConnect(),$commit);
   }
   function updateBonus($idCueilleur,$bonus)
   {
   $update="UPDATE cueilleurs set bonus=bonus+'%d' where idCueilleur=".$idCueilleur;
   $commit=sprintf($update,$bonus);
   $insert=mysqli_query(DbConnect(),$commit);
   }
   function updateMinus($idCueilleur,$minus)
   {
   $update="UPDATE cueilleurs set minus=minus+'%d' where idCueilleur=".$idCueilleur;
   $commit=sprintf($update,$minus);
   $insert=mysqli_query(DbConnect(),$commit);
   }
   function getMois()
   {
   $requete="SELECT * FROM mois ORDER BY idMois";
   $verification=mysqli_query(DbConnect(),$requete);
   $resultats=mysqli_fetch_assoc($verification);
   $indice=0;
      while($resultats=mysqli_fetch_assoc($verification))
      {
      $valiny[$indice]=$resultats;
      $indice++;
      }
   return $valiny;
   }
   function updateMoisDeRegeneration($idMois,$isRegenerating)
   {
   $update="UPDATE mois set isRegenerating='%d' where idMois=".$idMois;
   $commit=sprintf($update,$isRegenerating);
   $insert=mysqli_query(DbConnect(),$commit);
   }
   function calculNombreJours($dateDebut, $dateFin) 
   {
   $joursOuvrables = 0;
   $jourCourant = strtotime($dateDebut);
   while ($jourCourant <= strtotime($dateFin)) 
   {
         $joursOuvrables++;
         $jourCourant = strtotime('+1 day', $jourCourant);
   }
   return $joursOuvrables;
   }
   function getSalaireCeuilleurById($idCueilleur)
   {
   $requete="SELECT *,(rendement*prix) as salaire FROM v_parcelle where idCueilleur=".$idCueilleur;
   $verification=mysqli_query(DbConnect(),$requete);
   $resultats=mysqli_fetch_assoc($verification);
   return $resultats;  
   }
   function getEstimationSalaire($idCueilleur,$dateDebut,$dateFin)
   {

      $salaire=getSalaireCeuilleurById($idCueilleur);
      $nbJours=calculNombreJours(date('Y-m-d', $dateDebut),$dateFin);
      return $salaire["salaire"]*$nbJours;
   }

if (isset($_POST["fonction"]) && $_POST["fonction"] === "CheckLogin") {
   if (isset($_POST["user"], $_POST["pwd"])) {
      echo json_encode(checkLogin($_POST["user"], $_POST["pwd"]));
   }
}

if(isset($_POST["fonction"]) && $_POST["fonction"] === "getThe"){
   echo json_encode(getThe());
} 

if(isset($_POST["fonction"]) && $_POST["fonction"] === "getParcelle"){
   echo json_encode(getParcelles());
}

if(isset($_POST["fonction"]) && $_POST["fonction"] === "getCueilleurs"){
   echo json_encode(getCueilleur());
} 

if(isset($_POST["fonction"]) && $_POST["fonction"] === "getSalaire"){
   echo json_encode(getSalaire());
} 

if(isset($_POST["fonction"]) && $_POST["fonction"] === "getCDepense"){
   echo json_encode(getCategoryDepense());
} 

if(isset($_POST["fonction"]) && $_POST["fonction"] === "getRendementbyId"){
   if(isset($_POST["id"])){
      echo json_encode(getRendementById($_POST["id"]));
   }
} 
if(isset($_POST["fonction"]) && $_POST["fonction"] === "getIdThebyParcelle"){
   if(isset($_POST["id"])){
      echo json_encode(getIdTheByParcelle($_POST["id"]));
   }
} 
if(isset($_POST["fonction"]) && $_POST["fonction"] === "setPoidsRendement"){
   if(isset($_POST["poids"], $_POST["idThe"])){
      echo json_encode(substractPoidsRendement($_POST["poids"], $_POST["idThe"]));
   }
} 
if(isset($_POST["fonction"]) && $_POST["fonction"] === "updateprix"){
   if(isset($_POST["id"], $_POST["price"])){
      updateTea($_POST["id"], $_POST["price"]);
   }
}
if(isset($_POST["fonction"]) && $_POST["fonction"] === "updateCueilleur"){
   if(isset($_POST["id"], $_POST["minus"], $_POST["malus"], $_POST["bonus"])){
      updateMalus($_POST["id"], $_POST["malus"]);
      updateMinus($_POST["id"], $_POST["minus"]);
      updateBonus($_POST["id"], $_POST["bonus"]);
   }
}
if(isset($_POST["fonction"]) && $_POST["fonction"] === "updatemoisregen"){
   if(isset($_POST["id"], $_POST["regen"])){
      updateMoisDeRegeneration($_POST["id"], $_POST["regen"]);
   }
}
if(isset($_POST["fonction"]) && $_POST["fonction"] === "getMois"){
      echo json_encode(getMois());
}
if(isset($_POST["fonction"]) && $_POST["fonction"] === "doresult"){
   if(isset($_POST["id"], $_POST["DateDebut"], $_POST["DateFin"])){
      echo json_encode(getEstimationSalaire($_POST["id"], $_POST["DateDebut"], $_POST["DateFin"]));
   }
}
?>
document.addEventListener("DOMContentLoaded", function(){
    getDepense();
    var button = document.getElementById("confirm");
    button.addEventListener("click", function(event){
        event.preventDefault();
        var from = document.getElementById("form").value;
        var till = document.getElementById("till").value;
        var type_depense = document.getElementById("depense").value;
        var price = document.getElementById("price").value;
        var days = daysBetweenDates(from,till);
        if(days != -1){
            var array = {days: days,td: type_depense,price: price};
            sessionStorage.setItem('form-array2', JSON.stringify(array));
            window.location.href = "page_result.html";
        }
    })
})
function Selectpicker(parcelle){
    var selectElement = document.getElementById("depense");

    for (var i = 0; i < parcelle.length; i++) {
        var option = document.createElement("option");
        option.value = parcelle[i].idDepense;
        option.text = parcelle[i].nom; 
        selectElement.appendChild(option);
    }
}
function getxhr()
{
    var xhr; 
            try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
            catch (e) 
            {
                try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
                catch (e2) 
                {
                try {  xhr = new XMLHttpRequest();  }
                catch (e3) {  xhr = false;   }
                }
            }
    return xhr;
}

function getDepense(){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=getCDepense";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var parcelle = JSON.parse(xhr.responseText);
                console.log(parcelle);
                Selectpicker(parcelle);
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

function daysBetweenDates(startDate, endDate) {
    startDate = new Date(startDate);
    endDate = new Date(endDate);
    var daysDifference;

    // Convert both dates to milliseconds
    var startDateMS = startDate.getTime();
    var endDateMS = endDate.getTime();
    if(startDateMS > endDateMS){
        alert("Recheck your date please");
        return daysDifference = -1;
    }else{
        var differenceMS = endDateMS - startDateMS;
        daysDifference = Math.floor(differenceMS / (1000 * 60 * 60 * 24));

        return daysDifference;
    }
    
}
document.addEventListener("DOMContentLoaded", function(){
    getPicker();
    var button = document.getElementById("confirm");
    button.addEventListener("click", function(event){
        event.preventDefault();
        var id = document.getElementById("tea").value;
        var min = document.getElementById("weight").value;
        var bonus = document.getElementById("Bonus").value;
        var malus = document.getElementById("Malus").value;
        var button = document.getElementById("confirm");
        button.addEventListener("click", function(event){
            event.preventDefault();

            updateCueilleurs(id, malus, bonus, min);
            window.location.href = "page_admin.html";
        })
    })
})

function getxhr()
{
    var xhr; 
            try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
            catch (e) 
            {
                try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
                catch (e2) 
                {
                try {  xhr = new XMLHttpRequest();  }
                catch (e3) {  xhr = false;   }
                }
            }
    return xhr;
}

function SelectCueilleur(picker) {
    var selectElement = document.getElementById("tea");

    for (var i = 0; i < picker.length; i++) {
        var option = document.createElement("option");
        option.value = picker[i].idCueilleur;
        option.text = picker[i].nom; 
        selectElement.appendChild(option);
    }
}

function getPicker(){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var  data = "fonction=getCueilleurs";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var the = JSON.parse(xhr.responseText);
                SelectCueilleur(the);
            }
        }
    };
    xhr.send(data);
}

function updateCueilleurs(id, malus, bonus, minus){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var  data = "fonction=updateCueilleur&id="+encodeURIComponent(id)
    +"&minus="+encodeURIComponent(minus)
    +"&malus="+encodeURIComponent(malus)
    +"&bonus="+encodeURIComponent(bonus);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                alert("Confirmed");
            }
        }
    };
    xhr.send(data);
}
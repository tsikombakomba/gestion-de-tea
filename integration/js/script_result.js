document.addEventListener("DOMContentLoaded", function(){
    getDepense();
})
function getxhr()
{
var xhr; 
        try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
        catch (e) 
        {
            try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
            catch (e2) 
            {
            try {  xhr = new XMLHttpRequest();  }
            catch (e3) {  xhr = false;   }
            }
        }
return xhr;
}

function result(){
    var array1 = JSON.parse(sessionStorage.getItem("form-array"));
    var array2 = JSON.parse(sessionStorage.getItem("form-array2"));
    function updateDepenseCallback(count) {
        // This code will be executed once the getIdThebyIdParcelle function is completed
        // Use the returned value here
        updateDepense(count, array1["poids"]);
    }

    // Call getIdThebyIdParcelle with the defined callback function
    getIdThebyIdParcelle(array1["parcelle"], updateDepenseCallback);
}


function updateDepense(id, poids){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=setPoidsRendement&poids="+encodeURIComponent(poids)
    +"&idThe="+encodeURIComponent(id);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){     
                var box = document.getElementById("Result");
                var result = JSON.parse(xhr.responseText);
                var text2 = document.createElement("p");
                text2.classList.add("display-6");
                text2.innerHTML = "Poids retirer sur la Parcelle : "+result["rendement"];
                box.appendChild(text2);
                console.log(result);   
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

function getDepense(){
    var array1 = JSON.parse(sessionStorage.getItem("form-array"));
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=getRendementbyId&id="+array1["parcelle"];
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var parcelle = JSON.parse(xhr.responseText);
                // console.log(parcelle);
                var box = document.getElementById("Result");
                var array2 = JSON.parse(sessionStorage.getItem("form-array2"));
                var cout = (array1["poids"]*1000)+parseInt(array2["price"]);
                var text1 = document.createElement("p");
                text1.classList.add("display-6");
                text1.innerHTML = "Poids total :"+array1["poids"];
                box.appendChild(text1);
                
                var text3 = document.createElement("p");
                text3.classList.add("display-6");
                text3.innerHTML = "Cout de revient :"+cout;
                box.appendChild(text3);

                var text4 = document.createElement("p");
                text4.classList.add("display-6");
                text4.innerHTML = "Poids restant sur la Parcelle :"+parcelle["rendement"];
                box.appendChild(text4);

                result();
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

function getIdThebyIdParcelle(id, callback){
    xhr = getxhr();
    var count;
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=getIdThebyParcelle&id="+id;
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var parcelle = JSON.parse(xhr.responseText);
                count = parcelle["idThe"];
                callback(count);
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

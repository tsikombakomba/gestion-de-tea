document.addEventListener("DOMContentLoaded", function(){
    getCueilleurs();
    var button = document.getElementById("confirm");
    button.addEventListener("click", function(event){
        event.preventDefault();
        var picker = document.getElementsByClassName("cueilleurs");
        var datedebut = document.getElementById("dateDebut").value;
        var dateFin = document.getElementById("dateFin").value;
        for(var i=0; i<picker.length; i++){
            var ver = picker[i];
            console.log(ver.textContent);
            getEstimationSalaire("Salaire"+i, ver.textContent ,datedebut, dateFin);
        }
    })
})

function getxhr()
{
    var xhr; 
            try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
            catch (e) 
            {
                try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
                catch (e2) 
                {
                try {  xhr = new XMLHttpRequest();  }
                catch (e3) {  xhr = false;   }
                }
            }
    return xhr;
}

function getCueilleurs(){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=getCueilleurs";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var pass = JSON.parse(xhr.responseText);
                var box = document.getElementById("cueilleurs");
                var title = document.createElement("h2");
                console.log(pass);
                box.appendChild(title);
                while(box.firstChild){
                    box.removeChild(box.firstChild);
                }
                var table = document.createElement("table");
                table.classList.add('table');

                // Create thead element and append table headers
                const thead = document.createElement('thead');
                const headerRow = document.createElement('tr');
                ['Id','Nom', 'Bonus', 'Malus', 'Weight'].forEach(headerText => {
                    const th = document.createElement('th');
                    th.setAttribute('scope', 'col');
                    th.textContent = headerText;
                    headerRow.appendChild(th);
                });
                thead.appendChild(headerRow);
                table.appendChild(thead);

                // Create tbody element and append table rows with data
                const tbody = document.createElement('tbody');
                pass.forEach(item => {
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <th class="cueilleurs" scope="row">${item.idCueilleur}</th>
                    <td>${item.nom}</td>
                    <td>${item.bonus}</td>
                    <td>${item.malus}</td>
                    <td>${item.minus}</td>
                    `;
                    tbody.appendChild(row);
                });
                table.appendChild(tbody);

                box.appendChild(table);
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

function daysBetweenDates(startDate, endDate) {
    startDate = new Date(startDate);
    endDate = new Date(endDate);
    var daysDifference;

    // Convert both dates to milliseconds
    var startDateMS = startDate.getTime();
    var endDateMS = endDate.getTime();
    if(startDateMS > endDateMS){
        alert("Recheck your date please");
        return daysDifference = -1;
    }else{
        var differenceMS = endDateMS - startDateMS;
        daysDifference = Math.floor(differenceMS / (1000 * 60 * 60 * 24));

        return daysDifference;
    }
    
}

function getEstimationSalaire(nomsession,id, dateDebut, dateFin){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=doresult&id="+encodeURIComponent(id)
    +"&DateDebut="+encodeURIComponent(dateDebut)
    +"&DateFin="+encodeURIComponent(dateFin);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var result = JSON.parse(xhr.responseText);
                sessionStorage.setItem(nomsession, JSON.stringify(result));
            }
        }
    }
    xhr.send(data);
}
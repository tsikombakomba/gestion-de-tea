document.addEventListener("DOMContentLoaded", function(){
    var button = document.getElementById("login-button");
    button.addEventListener('click', function(event){
        event.preventDefault();

        var user = document.getElementById("username").value;
        var pwd = document.getElementById("password").value;
        CheckLogin(user, pwd);
    })
    var username = document.getElementById("username");
    var pwd = document.getElementById("password");
    var swch = document.getElementById("switch");
    var state = document.getElementById("state");

    swch.addEventListener("click", function() {
        if (state.value==0) {
            state.value=1;
            swch.textContent = "Admin mode";
            username.value = "admin";
            pwd.value = "admin1234";
        } else {
            state.value=0;
            swch.textContent = "Normal mode";
            username.value = "ronny";
            pwd.value = "ronny1234";
        }
    });
})

function getxhr()
{
    var xhr; 
            try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
            catch (e) 
            {
                try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
                catch (e2) 
                {
                try {  xhr = new XMLHttpRequest();  }
                catch (e3) {  xhr = false;   }
                }
            }
    return xhr;
}
function CheckLogin(user, pwd){
    var xhr = getxhr();
    console.log(user+" "+pwd);
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    
    var data = "fonction=CheckLogin&user="
    +encodeURIComponent(user)+
    "&pwd="+encodeURIComponent(pwd);

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var username = JSON.parse(xhr.responseText);
                if(username["idUser"]!=0){    
                    if(username["isAdmin"]==true){
                        window.location.href= 'page_admin.html';
                        sessionStorage.setItem('user', JSON.stringify(username));
                    }
                    else if(username["isAdmin"]==false){
                        window.location.href= 'page_home.html';
                        sessionStorage.setItem('user', JSON.stringify(username));
                    }
                    else{
                        alert("Error d'identifiant ou Mots de passe !");
                    }
                }
            }
            else{
                alert("Error d'identifiant ou Mots de passe...");
            }
        }
    };
    xhr.send(data);
}

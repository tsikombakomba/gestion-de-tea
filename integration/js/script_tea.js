document.addEventListener("DOMContentLoaded", function(){
    getThe();
    var button = document.getElementById("confirm");
    button.addEventListener("click", function(event){
        event.preventDefault();

        var tea = document.getElementById("tea").value;
        var price = document.getElementById("price").value;
        updateThe(tea, price);
        window.location.href = "page_admin.html"
    })
})

function getxhr()
{
    var xhr; 
            try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
            catch (e) 
            {
                try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
                catch (e2) 
                {
                try {  xhr = new XMLHttpRequest();  }
                catch (e3) {  xhr = false;   }
                }
            }
    return xhr;
}

function Selecttea(picker) {
    var selectElement = document.getElementById("tea");

    for (var i = 0; i < picker.length; i++) {
        var option = document.createElement("option");
        option.value = picker[i].idThe;
        option.text = picker[i].nom; 
        selectElement.appendChild(option);
    }
}

function getThe(){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var  data = "fonction=getThe";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var the = JSON.parse(xhr.responseText);
                Selecttea(the);
            }
        }
    };
    xhr.send(data);
}

function updateThe(id, price){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var  data = "fonction=updateprix&id="+encodeURIComponent(id)
    +"&price="+encodeURIComponent(price);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                alert("Confirmed");
            }
        }
    };
    xhr.send(data);
}
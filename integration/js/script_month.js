window.addEventListener("load", function(){
    document.getElementById("tea").value = 0;
    var button = document.getElementById("confirm");
    button.addEventListener("click", function(event){
        var select = document.getElementById("tea").value;
        event.preventDefault();
        var check = document.getElementsByClassName("form-check-input");
        
        for(var i=0;i<check.length;i++){
            var ver = check[i];
            if(ver.checked){
                console.log(ver.value);
                updateMoisDeRegeneration(ver.value, select);
                window.location.href = "page_admin.html";
            }
        }
    })
})

function getxhr()
{
    var xhr; 
            try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
            catch (e) 
            {
                try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
                catch (e2) 
                {
                try {  xhr = new XMLHttpRequest();  }
                catch (e3) {  xhr = false;   }
                }
            }
    return xhr;
}

function updateMoisDeRegeneration(id, isregen){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var  data = "fonction=updatemoisregen&id="+encodeURIComponent(id)
    +"&regen="+encodeURIComponent(isregen);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                alert("Confirmed");
            }
        }
    };
    xhr.send(data);
}
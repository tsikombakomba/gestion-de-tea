document.addEventListener("DOMContentLoaded", function(){
    var logout = document.getElementById("logout");
    logout.addEventListener('click', function(event) {
        event.preventDefault();
    
        sessionStorage.clear();
    
        window.location.href = "index.html";
    });
})
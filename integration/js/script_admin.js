document.addEventListener("DOMContentLoaded", function(){
    getThe();
    var select = document.getElementById("option-1");
    var box = document.getElementById("info");
    var logout = document.getElementById("logout");
    select.addEventListener("change", function(event){
        event.preventDefault();

        var value = select.value;
        if(value==1){
            while(box.firstChild){
                box.removeChild(box.firstChild);
            }
            getThe();     
        }
        if(value==2){
            while(box.firstChild){
                box.removeChild(box.firstChild);
            }
            getParcelle();
        }
        if(value==3){
            while(box.firstChild){
                box.removeChild(box.firstChild);
            }
            getCueilleurs();
        }
        if(value==4){
            while(box.firstChild){
                box.removeChild(box.firstChild);
            }
            getSalaire();
        }
        if(value==5){
            while(box.firstChild){
                box.removeChild(box.firstChild);
            }
            getDepense();
        }
    })
})

function getxhr()
{
    var xhr; 
            try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
            catch (e) 
            {
                try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
                catch (e2) 
                {
                try {  xhr = new XMLHttpRequest();  }
                catch (e3) {  xhr = false;   }
                }
            }
    return xhr;
}
function getThe(){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var  data = "fonction=getThe";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var the = JSON.parse(xhr.responseText);
                var box = document.getElementById("info");
                console.log(the);
                while(box.firstChild){
                    box.removeChild(box.firstChild);
                }
                var table = document.createElement("table");
                table.classList.add('table');

                // Create thead element and append table headers
                const thead = document.createElement('thead');
                const headerRow = document.createElement('tr');
                ['Id','Name', 'Occupation in m2', 'rendement', 'price'].forEach(headerText => {
                    const th = document.createElement('th');
                    th.setAttribute('scope', 'col');
                    th.textContent = headerText;
                    headerRow.appendChild(th);
                });
                thead.appendChild(headerRow);
                table.appendChild(thead);

                // Create tbody element and append table rows with data
                const tbody = document.createElement('tbody');
                the.forEach(item => {
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <th scope="row">${item.idThe}</th>
                    <td>${item.nom}</td>
                    <td>${item.occupation}</td>
                    <td>${item.rendement}</td>
                    <td>${item.prix}</td>
                    `;
                    tbody.appendChild(row);
                });
                table.appendChild(tbody);

                box.appendChild(table);
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

function getParcelle(){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=getParcelle";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var pass = JSON.parse(xhr.responseText);
                var box = document.getElementById("info");
                var title = document.createElement("h2");
                console.log(pass);
                box.appendChild(title);
                while(box.firstChild){
                    box.removeChild(box.firstChild);
                }
                var table = document.createElement("table");
                table.classList.add('table');

                // Create thead element and append table headers
                const thead = document.createElement('thead');
                const headerRow = document.createElement('tr');
                ['Id','Surface', 'Id-The', 'Nom du The', 'occupation', 'rendement', 'prix', 'cueilleurs'].forEach(headerText => {
                    const th = document.createElement('th');
                    th.setAttribute('scope', 'col');
                    th.textContent = headerText;
                    headerRow.appendChild(th);
                });
                thead.appendChild(headerRow);
                table.appendChild(thead);

                // Create tbody element and append table rows with data
                const tbody = document.createElement('tbody');
                pass.forEach(item => {
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <th scope="row">${item.idParcelle}</th>
                    <td>${item.surface}</td>
                    <td>${item.idThe}</td>
                    <td>${item.the}</td>
                    <td>${item.occupation}</td>
                    <td>${item.rendement}</td>
                    <td>${item.prix}</td>
                    <td>${item.cueilleurs}</td>
                    `;
                    tbody.appendChild(row);
                });
                table.appendChild(tbody);

                box.appendChild(table);
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

function getCueilleurs(){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=getCueilleurs";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var pass = JSON.parse(xhr.responseText);
                var box = document.getElementById("info");
                var title = document.createElement("h2");
                console.log(pass);
                box.appendChild(title);
                while(box.firstChild){
                    box.removeChild(box.firstChild);
                }
                var table = document.createElement("table");
                table.classList.add('table');

                // Create thead element and append table headers
                const thead = document.createElement('thead');
                const headerRow = document.createElement('tr');
                ['Id','Nom', 'Genre', 'Date de Naissance', 'rendement', 'bonus', 'malus', 'minus'].forEach(headerText => {
                    const th = document.createElement('th');
                    th.setAttribute('scope', 'col');
                    th.textContent = headerText;
                    headerRow.appendChild(th);
                });
                thead.appendChild(headerRow);
                table.appendChild(thead);

                // Create tbody element and append table rows with data
                const tbody = document.createElement('tbody');
                pass.forEach(item => {
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <th scope="row">${item.idCueilleur}</th>
                    <td>${item.nom}</td>
                    <td>${item.genre}</td>
                    <td>${item.dtn}</td>
                    <td>${item.rendement}</td>
                    <td>${item.bonus}</td>
                    <td>${item.malus}</td>
                    <td>${item.minus}</td>
                    `;
                    tbody.appendChild(row);
                });
                table.appendChild(tbody);

                box.appendChild(table);
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

function getSalaire(){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=getSalaire";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var pass = JSON.parse(xhr.responseText);
                var box = document.getElementById("info");
                var title = document.createElement("h2");
                console.log(pass);
                box.appendChild(title);
                while(box.firstChild){
                    box.removeChild(box.firstChild);
                }
                var table = document.createElement("table");
                table.classList.add('table');

                // Create thead element and append table headers
                const thead = document.createElement('thead');
                const headerRow = document.createElement('tr');
                ['Id','Nom', 'rendement', 'salaire'].forEach(headerText => {
                    const th = document.createElement('th');
                    th.setAttribute('scope', 'col');
                    th.textContent = headerText;
                    headerRow.appendChild(th);
                });
                thead.appendChild(headerRow);
                table.appendChild(thead);

                // Create tbody element and append table rows with data
                const tbody = document.createElement('tbody');
                pass.forEach(item => {
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <th scope="row">${item.idParcelle}</th>
                    <td>${item.cueilleurs}</td>
                    <td>${item.rendement}</td>
                    <td>${item.salaire}</td>
                    `;
                    tbody.appendChild(row);
                });
                table.appendChild(tbody);

                box.appendChild(table);
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

function getDepense(){
    xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var data = "fonction=getCDepense";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var pass = JSON.parse(xhr.responseText);
                var box = document.getElementById("info");
                var title = document.createElement("h2");
                console.log(pass);
                box.appendChild(title);
                while(box.firstChild){
                    box.removeChild(box.firstChild);
                }
                var table = document.createElement("table");
                table.classList.add('table');

                // Create thead element and append table headers
                const thead = document.createElement('thead');
                const headerRow = document.createElement('tr');
                ['Id','Nom'].forEach(headerText => {
                    const th = document.createElement('th');
                    th.setAttribute('scope', 'col');
                    th.textContent = headerText;
                    headerRow.appendChild(th);
                });
                thead.appendChild(headerRow);
                table.appendChild(thead);

                // Create tbody element and append table rows with data
                const tbody = document.createElement('tbody');
                pass.forEach(item => {
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <th scope="row">${item.idDepense}</th>
                    <td>${item.nom}</td>
                    `;
                    tbody.appendChild(row);
                });
                table.appendChild(tbody);

                box.appendChild(table);
            }
            else{
                alert("Couldn't not make the action");
            }
        }
    };
    xhr.send(data);
}

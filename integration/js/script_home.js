document.addEventListener("DOMContentLoaded", function(){
    getParcelle();
    getPicker();
    const slider = document.getElementById('slider');
    const sliderValue = document.getElementById('sliderValue');
    var parcelle = document.getElementById("parcelle");

    parcelle.addEventListener("click", function(){
        var id = parcelle.value;
        getRendementById(id);
    });

    slider.addEventListener('input', function () {
        sliderValue.textContent = slider.value;
    });


    var button=document.getElementById("confirm");
    button.addEventListener("click", function(event){
        event.preventDefault();

        var date = document.getElementById("date").value;
        var picker = document.getElementById("picker").value;
        var parcelle = document.getElementById("parcelle").value;
        var poids = document.getElementById("slider").value;

        if(date!=null && picker!=null && parcelle!=null && poids!=null){
            var array ={dtn: date, picker: picker, parcelle: parcelle, poids: poids};
            sessionStorage.setItem('form-array', JSON.stringify(array));
            window.location.href = "page_depense.html";
        }
        else{
            alert("Please fill every field");
        }
    });
})

function Selectpicker(picker) {
    var selectElement = document.getElementById("picker");

    for (var i = 0; i < picker.length; i++) {
        var option = document.createElement("option");
        option.value = picker[i].idCueilleur;
        option.text = picker[i].nom; 
        selectElement.appendChild(option);
    }
}
function SelectParcelle(parcelle){
    var selectElement = document.getElementById("parcelle");

    for (var i = 0; i < parcelle.length; i++) {
        var option = document.createElement("option");
        option.value = parcelle[i].idParcelle;
        option.text = "parcelle "+parcelle[i].idParcelle; 
        selectElement.appendChild(option);
    }
}
function getxhr()
{
    var xhr; 
            try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
            catch (e) 
            {
                try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
                catch (e2) 
                {
                try {  xhr = new XMLHttpRequest();  }
                catch (e3) {  xhr = false;   }
                }
            }
    return xhr;
}

function getParcelle(){
    var xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    
    var data = "fonction=getParcelle";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var parcelle = JSON.parse(xhr.responseText);
                console.log(parcelle);
                SelectParcelle(parcelle);
            }
        }
    };
    xhr.send(data);
}
function getPicker(){
    var xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    var data = "fonction=getCueilleurs";
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var parcelle = JSON.parse(xhr.responseText);
                console.log(parcelle);
                Selectpicker(parcelle);
            }
        }
    };
    xhr.send(data);
}

function getRendementById(id){
    var xhr = getxhr();
    xhr.open("POST", "php/function.php", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    var data = "fonction=getRendementbyId&id="+encodeURIComponent(id);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                var parcelle = JSON.parse(xhr.responseText);
                console.log(parcelle);
                const slider = document.getElementById('slider');
                const sliderValue = document.getElementById('sliderValue');
                slider.max = parcelle["rendement"];
                sliderValue.textContent = slider.max;
            }
        }
    };
    xhr.send(data);
}


